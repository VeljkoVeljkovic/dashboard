<?php



// use Illuminate\Contracts\Auth\MustVerifyEmail;


 namespace App;

    use Illuminate\Notifications\Notifiable;
    use Illuminate\Database\Eloquent\SoftDeletes;
    use Illuminate\Foundation\Auth\User as Authenticatable;
    use Laravel\Passport\HasApiTokens;
    class User extends Authenticatable
    {
        use Notifiable, SoftDeletes, HasApiTokens;

        protected $fillable = [
            'id', 'firstname', 'lastname', 'companyname', 'country', 'streetaddress', 'postcode', 'town', 'email', 'phone', 'password'
        ];

        protected $hidden = [
            'password', 'remember_token',
        ];

       public function coupons() {
	  return $this->belongsToMany('App\Coupons');
  }
       public function orders() {
		 return $this->hasMany('App\Order');
	   }

    }
