<?php
namespace App\Services;
use App\Order;
use App\User;
use App\Coupons;

class OrderServices {
function order($request) {

    if(!$request->id) {

        $data = $request->only(['firstname','lastname', 'companyname', 'country', 'streetaddress', 'postcode', 'town', 'phone', 'email', 'password']);
        if($data['password'] != 0) {$data['password'] = bcrypt($data['password']);}
        else {$data['password'] = ""; }
        $user = User::create($data);
        $user_id = $user->id;
              } else {$user_id = $request->id;}

              foreach($request->product as $p) {
               if($p['discount_price']) {
                   $coupon = Coupons::find($request->coupon);
                   $coupon->users()->detach($user_id);
               }
               $order = Order::create([
               'product_id' => $p['id'],
               'price' => $p['price'],
               'discount_price' => $p['discount_price'],
               'quantity' => $p['quantity'],
               'user_id' => $user_id
          ]);}

}

function delivery($u) {  
    foreach($u as $o) {
    $order =  Order::where('id', $o->id);

    $status = $order->update([
    'is_delivered' => 1
    ]);
}
}

}