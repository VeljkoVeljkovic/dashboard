<?php
namespace App\Services;
use App\Jobs\UploadImageJob;
use Illuminate\Support\Str;
class ProductServices {

function upladImage($image) {

    $exploded =explode(',',  $image);	
		if(Str::contains($exploded[0], 'jpeg')){
			$extension = 'jpg';
		} else {
			$extension = 'png';
        }
        $fileName = time().".".$extension;	      

        $imageData = array(
            'name' => $fileName,
            'decoded' => $exploded[1],
          );
          dispatch(new UploadImageJob($imageData));
          return $imageData['name'];
}

 function format($request, $imageName) {
    return [
      'name' => $request->name,
      'description' => $request->description,
      'category_id' => $request->category_id,
      'brand_id' => $request->brand_id,
      'tag_id' => $request->tag_id,
      'size' => $request->size,
      'color' => $request->color,
      'units' => $request->units,
      'price' => $request->price,			
      'image' => $imageName
    ];
}
}