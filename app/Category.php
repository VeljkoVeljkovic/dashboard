<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
  protected $fillable = [ 'id', 'kategorija'];

    public function productcategory(){
            return $this->hasMany(Product::class);
        }

    public function children()
        {
        return $this->hasMany('App\Category', 'id');
        }
 
}
