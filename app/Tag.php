<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    // protected $fillable = [ 'id', 'tag'];
     protected $guarded = [];
    public function producttag(){
            return $this->hasMany(Product::class);
        }
}
