<?php

namespace App\Http\Controllers;

use App\Brand;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         return response()->json(Brand::all());
    }

 

    public function store(Request $request)
    {
        $brand = new Brand([
      'brand' => $request->get('brand')
    ]);

    $brand->save();

    return response()->json('successfully added');
    }


    public function update(Request $request, $id)
    {
        $brand = Brand::findOrFail($id);

    $brand->update($request->all());

    return response()->json('successfully updated');
    }

   
    public function delete($id)
    {
         $brand = Brand::findOrFail($id);

    $brand->delete();

    return response()->json(['message'=>'Brand deleted'], 200);
    }
}
