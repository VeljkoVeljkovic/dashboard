<?php

 namespace App\Http\Controllers;
    use Illuminate\Support\Facades\DB;	
	use App\User;
    use App\Order;
    use Auth;
    

    class ChartController extends Controller
    {
        public function index()
        {
            return response()->json(User::orderBy('id', 'desc')->get(),200);
        }
		
	
		
		public function sales() {
	       $from = date('2019-01-01');
                    $to = date('2019-12-31');
			         $chart = Order::select(DB::raw('DATE(created_at)as `date`'), DB::raw('sum(quantity) as `data`'),   DB::raw('YEAR(created_at) year, MONTH(created_at) month'))
					->whereBetween('created_at', [$from, $to])
				    ->groupby('year','month')					
					->get()
					->pluck('data', 'date');

			 return response()->json(['chart'=>$chart]);
		}
		
		public function usery() {
		            $from = date('2019-01-01');
                    $to = date('2019-12-31');
			         $chart = Order::select(DB::raw('sum(quantity) as `data`'),   DB::raw('YEAR(created_at) year, MONTH(created_at) month'))
					->whereBetween('created_at', [$from, $to])
				    ->groupby('year','month')					
					 ->pluck('data');

			 return response()->json(['chart'=>$chart]);
		}
		
		public function pie() {
              
			 $chart = DB::table('orders')->select('categories.kategorija', DB::raw('sum(orders.quantity) as `data`'))
			 ->join('products', 'products.id', '=', 'orders.product_id' )
			 ->join('categories', 'categories.id', '=', 'products.category_id' )
			 ->groupby('products.category_id')
		     ->get('categories.kategorija', 'data'); 
			 $labels = DB::table('categories')->pluck('kategorija');
			 return response()->json(['chart'=>$chart, 'labels'=>$labels]);
		}
        public function geo() {
			$chart = User::select('country', DB::raw('count(id) as `c`'))->groupby('country')->get('country', 'c');
			
			return response()->json(['chart'=>$chart]);
		}
		
     
    }
