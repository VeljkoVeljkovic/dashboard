<?php

 namespace App\Http\Controllers; 
	use App\User;  
    use Auth;
    use Illuminate\Http\Request;
    use App\Services\OrderServices;

    class OrderController extends Controller
    {

        private $orderServices;

        public function __construct(OrderServices $orderServices)
        {
            $this->orderServices = $orderServices;
        } 


        public function index()
        {
            return response()->json(User::with('orders')->get(),200);
        }

		public function store(Request $request, OrderServices $orderServices)
        {

            $orderServices->order($request);
			return response()->json(['message'=>'Success']);
        }

        public function update($id, OrderServices $orderServices)
        {

            $user = User::where('id', $id)->with('orders')->first();
            $orderServices->delivery($user->orders);			
            return response()->json([
                'status' => 'Success'
            ]);
        }

       
    }
