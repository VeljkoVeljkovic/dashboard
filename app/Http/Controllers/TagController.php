<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {    $tag = Tag::orderBy('id', 'desc')->whereNotNull('tag')->get();
         return response()->json($tag, 200);
    }

    
    public function store(Request $request)
    {
        $tag = new Tag([
      'tag' => $request->get('tag')
    ]);

    $tag->save();

    return response()->json('successfully added');
    }

    
    public function update(Request $request, $id)
    {
        $tag = Tag::find($id);

    $tag->update($request->all());

    return response()->json('successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
         $tag = Tag::find($id);

    $tag->delete();

    return response()->json(['message'=>'Tag deleted'], 200);
    }
}
