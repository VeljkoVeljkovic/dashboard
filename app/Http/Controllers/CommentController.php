<?php

namespace App\Http\Controllers;
use Illuminate\Support\Str;
use App\Comment;
use Illuminate\Http\Request;
use App\Http\Resources\CommentCollection;
class CommentController extends Controller
{
	  public function index()
	{

		$comment = Comment::orderBy('id', 'desc')->where('status', null)->get();
		return response()->json(['comment'=>$comment], 200);

	}

	public function store(Request $request)
	{

	  $comment = new Comment($request->all());
	  $comment->save();
	   return response()->json('successfully added');
	}


	public function update(Request $request, $id)
	{
		$comment = Comment::findOrFail($id);

		$comment->update([
		'status' => 'odobren'
		]);
		return response()->json('successfully updated');
	}

	public function delete($id)
	{
		$comment = Comment::findOrFail($id);
		$comment->delete();
		 return response()->json('successfully deleted', 200);
	}
}
