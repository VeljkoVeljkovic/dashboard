<?php
namespace App\Http\Controllers;
use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
  
    public function index()
    {
       $categor = Category::all();
       return response()->json($categor,200);

    }

   
    public function store(Request $request)
    {
      if($request->get('p_id') == '') {
       $subcategory = 0;
      } else {
      $subcategory = $request->get('p_id');
      }
         $category = new Category([
      'kategorija' => $request->get('kategorija'),
      'p_id' =>  $subcategory

    ]);

    $category->save();

    return response()->json('successfully added');
    }

  
    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);

    $category->update($request->all());

    return response()->json('successfully updated');
    }

   
    public function delete($id)
    {
         $category = Category::findOrFail($id);

    $category->delete();

    return response()->json(['message'=>'Category deleted'], 200);
    }
}
