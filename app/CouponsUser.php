<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CouponsUser extends Model
{
    protected $table = 'coupons_user';

  protected $guarded = [];
  
   public function c() {
	  return $this->belongsTo('App\Coupons', 'coupons_id', 'id');
  }
  }
