<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupons extends Model
{
     protected $fillable = [ 'id', 'coupon', 'valid_until', 'amount', 'percent'];

    
		
		  public function users() {
	  return $this->belongsToMany('App\User');
  }
   
     public function couponsuser() {
	  return $this->hasMany('App\CouponsUser', 'coupons_id', 'id');
  }
     
}
