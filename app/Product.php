<?php

namespace App;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class Product extends Model
    {
        use SoftDeletes;

        protected $fillable = [
            'name', 'price','discount_price', 'brand_id', 'units', 'tag_id', 'category_id', 'description', 'image', 'color', 'size'
        ];

        public function sales(){
            return $this->hasMany(Order::class);
        }

         public function brand()
        {
            return $this->belongsTo(Brand::class);
        }
		 public function tag()
        {
            return $this->belongsTo(Tag::class);
        }

          public function categor()
        {
            return $this->belongsTo(Category::class, 'categor_id', 'p_id');
        }

       

    }
