<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateProductsTable extends Migration
    {
        public function up()
        {
            Schema::create('products', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
				$table->string('size');
				$table->string('color');
                $table->longText('description');
                $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')
                ->references('id')->on('categories')
                ->onDelete('cascade');
                $table->unsignedBigInteger('brand_id');
            $table->foreign('brand_id')
                ->references('id')->on('brands')
                ->onDelete('cascade');
	    	$table->unsignedBigInteger('tag_id');
				 $table->foreign('tag_id')
                ->references('id')->on('tags')
                ->onDelete('cascade');
                $table->integer('units')->default(0);
                $table->double('price');                
                $table->string('image');
                $table->timestamps();
                $table->softDeletes();
            });
        }

        public function down()
        {
            Schema::dropIfExists('products');
        }
    }
