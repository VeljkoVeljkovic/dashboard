<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Tag;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'firstname' => $faker->name,
		'lastname' => $faker->name,
		'companyname' => $faker->company,
		'country' => $faker->country,
		'streetaddress' => $faker->streetAddress,
		'postcode' => $faker->postcode,
		'town' => $faker->city,
		'phone' => $faker->e164PhoneNumber,
		'is_admin' => 0,
        'email' => $faker->unique()->safeEmail,        
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
		'size' => $faker->randomElement(['M', 'S', 'XL', 'XS', 'L']),
		'color' => $faker->randomElement(['black', 'blue', 'yellow', 'green', 'white', 'gray', 'red']),
		'description' => $faker->text($maxNbChars = 120),
		'category_id' => $faker->numberBetween($min = 1, $max = 10),
		'brand_id' => $faker->numberBetween($min = 1, $max = 10),
		'tag_id' => $faker->numberBetween($min = 1, $max = 10),
		'units' => $faker->numberBetween($min = 10, $max = 80),
		'price' => $faker->numberBetween($min = 15, $max = 120),
        'image' => $faker->image,                
    ];
});


$factory->define(App\Category::class, function (Faker $faker) {
    return [
        'kategorija' => $faker->randomElement(['mens', 'kids', 'women']),		
    ];
});

$factory->define(App\Brand::class, function (Faker $faker) {
    return [
        'brand' => $faker->randomElement(['Calvin Klein', 'Diesel', 'Polo', 'Tommy Hilfiger']),		
    ];
});

$factory->define(App\Tag::class, function (Faker $faker) {
    return [
        'tag' => $faker->name,		
    ];
});


$factory->define(App\Order::class, function (Faker $faker) {
    return [
        
		'product_id' => $faker->numberBetween($min = 101, $max = 200),
		'user_id' => $faker->numberBetween($min = 1, $max = 6),
		'quantity' => $faker->numberBetween($min = 4, $max = 10),
		'price' => $faker->numberBetween($min = 10, $max = 80),
		'discount_price' => $faker->boolean(50) ? rand(10,100) : null,
		'created_at' => $faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now', $timezone = null),
                     
    ];
});

$factory->define(App\Coupons::class, function (Faker $faker) {
    return [
        
		'coupon' => $faker->word,
		'valid_until' => $faker->dateTimeBetween($startDate = 'now', $endDate = '+ 2 months', $timezone = null),		
		'amount' => $faker->numberBetween($min = 5, $max = 10),
		
                     
    ];
});



$factory->define(App\CouponsUser::class, function (Faker $faker) {
    return [
        
		'coupons_id' => $faker->numberBetween($min = 1, $max = 9),
		'user_id' => $faker->numberBetween($min = 3, $max = 8),
		'coupongenerator' => $faker->numberBetween($min = 1000, $max = 8000),
		'created_at' => $faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now', $timezone = null),
                     
    ];
});

