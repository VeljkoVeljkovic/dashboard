(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[14],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/tables.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/tables.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2/dist/sweetalert2.js */ "./node_modules/sweetalert2/dist/sweetalert2.js");
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2/src/sweetalert2.scss */ "./node_modules/sweetalert2/src/sweetalert2.scss");
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_2__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      geo: [],
      geodata: [],
      products: [],
      categories: [],
      brands: [],
      product: {
        id: '',
        name: '',
        description: '',
        category_id: null,
        brand_id: null,
        tag_id: null,
        size: null,
        color: null,
        units: '',
        price: ''
      },
      category: {
        id: '',
        kategorija: ''
      },
      brand: {
        id: '',
        brand: ''
      },
      loaded: false,
      fields: [{
        key: 'name',
        label: 'Name',
        sortable: true,
        sortDirection: 'asc'
      }, {
        key: 'units',
        label: 'Units',
        sortable: true,
        sortDirection: 'asc'
      }, {
        key: 'price',
        label: 'Price',
        sortable: true,
        sortDirection: 'asc'
      }, {
        key: 'category_id',
        label: 'Category',
        sortable: true,
        sortDirection: 'asc'
      }, {
        key: 'brand_id',
        label: 'Brand',
        sortable: true,
        sortDirection: 'asc'
      }, {
        key: 'size',
        label: 'Size',
        sortable: true,
        sortDirection: 'asc'
      }, {
        key: 'color',
        label: 'Color',
        sortable: true,
        sortDirection: 'asc'
      }],
      totalRows: 1,
      currentPage: 1,
      perPage: 15,
      pageOptions: [10, 15, 25],
      sortBy: '',
      sortDesc: false,
      sortDirection: 'asc',
      filter: null,
      filterOn: [],
      autoComplete: ''
    };
  },
  created: function created() {},
  filters: {
    truncate: function truncate(content, length, suffix) {
      return content.substring(0, length) + suffix;
    }
  },
  computed: {
    filteredItems: function filteredItems() {
      var _this = this;

      return this.autocompleteItems.filter(function (i) {
        return i.text.toLowerCase().indexOf(_this.tag.toLowerCase()) !== -1;
      });
    },
    sortOptions: function sortOptions() {
      // Create an options list from our fields
      return this.fields.filter(function (f) {
        return f.sortable;
      }).map(function (f) {
        return {
          text: f.label,
          value: f.key
        };
      });
    }
  },
  mounted: function mounted() {
    var _this2 = this;

    axios.get('/api/products/', {
      headers: {
        'Access-Control-Allow-Origin': '*'
      }
    }).then(function (response) {
      _this2.products = response.data.product;
      _this2.brands = response.data.brands;
      _this2.categories = response.data.cat;
      _this2.totalRows = _this2.products.length;
    });
  },
  methods: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapActions"])(["start", "tagovi", "category", "notfilteredproducts"]), {
    onsuccess: function onsuccess() {
      sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
        position: 'top-end',
        icon: 'warning',
        title: this.callMessage,
        showConfirmButton: false,
        timer: 1000
      });
    },
    onFiltered: function onFiltered(filteredItems) {
      // Trigger pagination to update the number of buttons/pages due to filtering
      this.totalRows = filteredItems.length;
      this.currentPage = 1;
    }
  })
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/tables.vue?vue&type=template&id=662d6356&scoped=true&":
/*!***************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/tables.vue?vue&type=template&id=662d6356&scoped=true& ***!
  \***************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "container-fluid" }, [
      _c("h1", { staticClass: "mt-4" }, [_vm._v("Table")]),
      _vm._v(" "),
      _vm._m(0),
      _vm._v(" "),
      _c("div", { staticClass: "card mb-4" }, [
        _vm._m(1),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "card-body" },
          [
            _c(
              "b-row",
              [
                _c(
                  "b-col",
                  { staticClass: "my-1", attrs: { lg: "6" } },
                  [
                    _c(
                      "b-form-group",
                      {
                        staticClass: "mb-0",
                        attrs: {
                          label: "Filter",
                          "label-cols-sm": "3",
                          "label-align-sm": "right",
                          "label-size": "sm",
                          "label-for": "filterInput"
                        }
                      },
                      [
                        _c(
                          "b-input-group",
                          { attrs: { size: "sm" } },
                          [
                            _c("b-form-input", {
                              attrs: {
                                type: "search",
                                id: "filterInput",
                                placeholder: "Type to Search"
                              },
                              model: {
                                value: _vm.filter,
                                callback: function($$v) {
                                  _vm.filter = $$v
                                },
                                expression: "filter"
                              }
                            }),
                            _vm._v(" "),
                            _c(
                              "b-input-group-append",
                              [
                                _c(
                                  "b-button",
                                  {
                                    attrs: { disabled: !_vm.filter },
                                    on: {
                                      click: function($event) {
                                        _vm.filter = ""
                                      }
                                    }
                                  },
                                  [_vm._v("Clear")]
                                )
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "b-col",
                  { staticClass: "my-1", attrs: { sm: "5", md: "6" } },
                  [
                    _c(
                      "b-form-group",
                      {
                        staticClass: "mb-0",
                        attrs: {
                          label: "Per page",
                          "label-cols-sm": "6",
                          "label-cols-md": "4",
                          "label-cols-lg": "3",
                          "label-align-sm": "right",
                          "label-size": "sm",
                          "label-for": "perPageSelect"
                        }
                      },
                      [
                        _c("b-form-select", {
                          attrs: {
                            id: "perPageSelect",
                            size: "sm",
                            options: _vm.pageOptions
                          },
                          model: {
                            value: _vm.perPage,
                            callback: function($$v) {
                              _vm.perPage = $$v
                            },
                            expression: "perPage"
                          }
                        })
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c("b-table", {
              staticClass: "table table-hover table-dark",
              attrs: {
                "show-empty": "",
                small: "",
                stacked: "md",
                items: _vm.products,
                fields: _vm.fields,
                "current-page": _vm.currentPage,
                "per-page": _vm.perPage,
                filter: _vm.filter,
                filterIncludedFields: _vm.filterOn,
                "sort-by": _vm.sortBy,
                "sort-desc": _vm.sortDesc,
                "sort-direction": _vm.sortDirection
              },
              on: {
                "update:sortBy": function($event) {
                  _vm.sortBy = $event
                },
                "update:sort-by": function($event) {
                  _vm.sortBy = $event
                },
                "update:sortDesc": function($event) {
                  _vm.sortDesc = $event
                },
                "update:sort-desc": function($event) {
                  _vm.sortDesc = $event
                },
                filtered: _vm.onFiltered
              },
              scopedSlots: _vm._u([
                {
                  key: "cell(category_id)",
                  fn: function(row) {
                    return _vm._l(_vm.categories, function(category) {
                      return _c("div", [
                        category.id === row.item.category_id
                          ? _c("div", [
                              _vm._v(
                                "\n                        " +
                                  _vm._s(category.kategorija) +
                                  "\n                    "
                              )
                            ])
                          : _vm._e()
                      ])
                    })
                  }
                },
                {
                  key: "cell(brand_id)",
                  fn: function(row) {
                    return _vm._l(_vm.brands, function(brand) {
                      return _c("div", [
                        brand.id === row.item.brand_id
                          ? _c("div", [
                              _vm._v(
                                "\n                        " +
                                  _vm._s(brand.brand) +
                                  "\n                    "
                              )
                            ])
                          : _vm._e()
                      ])
                    })
                  }
                }
              ])
            }),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "d-flex justify-content-center" },
              [
                _c("b-pagination", {
                  staticClass: "justify-content-center",
                  attrs: {
                    "total-rows": _vm.totalRows,
                    "per-page": _vm.perPage,
                    align: "fill",
                    size: "sm"
                  },
                  model: {
                    value: _vm.currentPage,
                    callback: function($$v) {
                      _vm.currentPage = $$v
                    },
                    expression: "currentPage"
                  }
                })
              ],
              1
            )
          ],
          1
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ol", { staticClass: "breadcrumb mb-4" }, [
      _c("li", { staticClass: "breadcrumb-item active" }, [_vm._v("Table")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-header" }, [
      _c("i", { staticClass: "fas fa-table mr-1" }),
      _vm._v("DataTable Example")
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/front/tables.vue":
/*!**************************************************!*\
  !*** ./resources/js/components/front/tables.vue ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _tables_vue_vue_type_template_id_662d6356_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./tables.vue?vue&type=template&id=662d6356&scoped=true& */ "./resources/js/components/front/tables.vue?vue&type=template&id=662d6356&scoped=true&");
/* harmony import */ var _tables_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./tables.vue?vue&type=script&lang=js& */ "./resources/js/components/front/tables.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _tables_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _tables_vue_vue_type_template_id_662d6356_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _tables_vue_vue_type_template_id_662d6356_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "662d6356",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/front/tables.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/front/tables.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/components/front/tables.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_tables_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./tables.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/tables.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_tables_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/front/tables.vue?vue&type=template&id=662d6356&scoped=true&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/components/front/tables.vue?vue&type=template&id=662d6356&scoped=true& ***!
  \*********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_tables_vue_vue_type_template_id_662d6356_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./tables.vue?vue&type=template&id=662d6356&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/tables.vue?vue&type=template&id=662d6356&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_tables_vue_vue_type_template_id_662d6356_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_tables_vue_vue_type_template_id_662d6356_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);