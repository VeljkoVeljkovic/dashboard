(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[10],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Category.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Category.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! sweetalert2/dist/sweetalert2.js */ "./node_modules/sweetalert2/dist/sweetalert2.js");
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2/src/sweetalert2.scss */ "./node_modules/sweetalert2/src/sweetalert2.scss");
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      //   isLoggedIn : null,
      categories: [],
      category: {
        id: '',
        kategorija: '',
        p_id: ''
      },
      category_id: '',
      edit: false
    };
  },
  mounted: function mounted() {//    this.isLoggedIn = localStorage.getItem('shop.mdi.in.rs.jwt') != null;
  },
  created: function created() {
    this.sveKategorije();
  },
  methods: {
    sveKategorije: function sveKategorije() {
      var _this = this;

      var uri = '/api/categories';
      this.axios.get(uri).then(function (response) {
        _this.categories = response.data;
      });
    },
    stopEdit: function stopEdit() {
      this.edit = false;
    },
    deleteCategory: function deleteCategory(id) {
      var _this2 = this;

      var swalWithBootstrapButtons = sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      });
      swalWithBootstrapButtons.fire({
        title: 'Da li ste sigurni ?',
        text: "Želite da obrišete ovu kategoriju!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Da, obriši je!',
        cancelButtonText: 'Ne!',
        reverseButtons: true
      }).then(function (result) {
        if (result.value) {
          var uri = "/api/category/delete/".concat(id);

          _this2.axios["delete"](uri, {
            headers: {
              'Access-Control-Allow-Origin': '*'
            }
          }).then(function (response) {
            swalWithBootstrapButtons.fire('Obrisan!', 'Vaša kategorija je obrisana.', 'success');

            _this2.clearForm();

            _this2.sveKategorije();
          });
        } else if (
        /* Read more about handling dismissals below */
        result.dismiss === sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.DismissReason.cancel) {
          swalWithBootstrapButtons.fire('Otkazano', 'Vaša kategorija je i dalje aktivna', 'info');
        }
      });
    },
    addCategory: function addCategory() {
      var _this3 = this;

      if (this.edit === false) {
        var uri = '/api/category/create';
        this.axios.post(uri, this.category).then(function (response) {
          _this3.clearForm();

          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Kategorija je dodata!',
            showConfirmButton: false,
            timer: 1500
          });

          _this3.sveKategorije();
        });
      } else {
        // update Category
        var _uri = "/api/category/update/".concat(this.category.id);

        this.axios.post(_uri, this.category).then(function (response) {
          _this3.clearForm();

          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Kategorija je izmenjena!',
            showConfirmButton: false,
            timer: 1500
          });

          _this3.sveKategorije();
        });
      }
    },
    editCategory: function editCategory(category) {
      this.edit = true;
      this.category.id = category.id;
      this.category.category_id = category.category_id;
      this.category.kategorija = category.kategorija;
      this.category.p_id = category.p_id;
    },
    clearForm: function clearForm() {
      this.edit = false;
      this.category.id = null;
      this.category.category_id = null;
      this.category.kategorija = null;
      this.category.p_id = null;
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Category.vue?vue&type=template&id=d1b3502e&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Category.vue?vue&type=template&id=d1b3502e& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container", attrs: { id: "rout" } }, [
    _c("br"),
    _c("br"),
    _c("br"),
    _vm._v(" "),
    _c("div", [
      _vm.edit
        ? _c("div", [_c("h1", [_vm._v("Izmeni Kategoriju:")])])
        : _c("div", [_c("h1", [_vm._v("Dodaj Kategoriju:")])]),
      _vm._v(" "),
      _c(
        "form",
        {
          on: {
            submit: function($event) {
              $event.preventDefault()
              return _vm.addCategory($event)
            }
          }
        },
        [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6" }, [
              _c("div", { staticClass: "form-group" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.category.kategorija,
                      expression: "category.kategorija"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text" },
                  domProps: { value: _vm.category.kategorija },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.category, "kategorija", $event.target.value)
                    }
                  }
                })
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-6" }, [
              _c(
                "div",
                { staticClass: "form-group" },
                [
                  _c(
                    "b-form-select",
                    {
                      model: {
                        value: _vm.category.p_id,
                        callback: function($$v) {
                          _vm.$set(_vm.category, "p_id", $$v)
                        },
                        expression: "category.p_id"
                      }
                    },
                    _vm._l(_vm.categories, function(category) {
                      return category.p_id == 0
                        ? _c("option", { domProps: { value: category.id } }, [
                            _vm._v(
                              "\n\n                                    " +
                                _vm._s(category.kategorija) +
                                "\n\n                                "
                            )
                          ])
                        : _vm._e()
                    }),
                    0
                  )
                ],
                1
              )
            ])
          ]),
          _vm._v(" "),
          _c("br"),
          _vm._v(" "),
          _vm.edit
            ? _c("div", { staticClass: "form-group" }, [
                _c("button", { staticClass: "site-btn place-btn" }, [
                  _vm._v("Izmeni")
                ]),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass: "site-btn place-btn",
                    on: {
                      click: function($event) {
                        $event.preventDefault()
                        return _vm.clearForm()
                      }
                    }
                  },
                  [_vm._v("Povratak na prethodnu stranu")]
                )
              ])
            : _c("div", { staticClass: "form-group" }, [
                _c("button", { staticClass: "site-btn place-btn" }, [
                  _vm._v("Kreiraj")
                ])
              ])
        ]
      )
    ]),
    _vm._v(" "),
    _c("table", { staticClass: "table table-hover" }, [
      _vm._m(0),
      _vm._v(" "),
      _c(
        "tbody",
        _vm._l(_vm.categories, function(category) {
          return _c("tr", { key: category.id }, [
            _c("td", [_vm._v(_vm._s(category.id))]),
            _vm._v(" "),
            _c("td", [_vm._v(_vm._s(category.kategorija))]),
            _vm._v(" "),
            _c("td", [
              category.p_id == 0 ? _c("div") : _vm._e(),
              _vm._v(" "),
              category.p_id != 0
                ? _c(
                    "div",
                    _vm._l(_vm.categories, function(c) {
                      return _c("div", { key: c.id }, [
                        c.id == category.p_id
                          ? _c("div", [
                              _vm._v(
                                "\n                                    " +
                                  _vm._s(c.kategorija) +
                                  "\n                                "
                              )
                            ])
                          : _vm._e()
                      ])
                    }),
                    0
                  )
                : _vm._e()
            ]),
            _vm._v(" "),
            _c("td", [
              _c(
                "button",
                {
                  staticClass: "p-btn",
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.editCategory(category)
                    }
                  }
                },
                [_vm._v("Edit")]
              ),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass: "p-btn",
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.deleteCategory(category.id)
                    }
                  }
                },
                [_vm._v("Delete")]
              )
            ])
          ])
        }),
        0
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("ID")]),
        _vm._v(" "),
        _c("th", [_vm._v("Kategorija")]),
        _vm._v(" "),
        _c("th", [_vm._v("Kategorija Roditelj")]),
        _vm._v(" "),
        _c("th", [_vm._v("Actions")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/admin/Category.vue":
/*!****************************************************!*\
  !*** ./resources/js/components/admin/Category.vue ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Category_vue_vue_type_template_id_d1b3502e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Category.vue?vue&type=template&id=d1b3502e& */ "./resources/js/components/admin/Category.vue?vue&type=template&id=d1b3502e&");
/* harmony import */ var _Category_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Category.vue?vue&type=script&lang=js& */ "./resources/js/components/admin/Category.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Category_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Category_vue_vue_type_template_id_d1b3502e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Category_vue_vue_type_template_id_d1b3502e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/admin/Category.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/admin/Category.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/js/components/admin/Category.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Category_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Category.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Category.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Category_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/admin/Category.vue?vue&type=template&id=d1b3502e&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/admin/Category.vue?vue&type=template&id=d1b3502e& ***!
  \***********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Category_vue_vue_type_template_id_d1b3502e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Category.vue?vue&type=template&id=d1b3502e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Category.vue?vue&type=template&id=d1b3502e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Category_vue_vue_type_template_id_d1b3502e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Category_vue_vue_type_template_id_d1b3502e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);