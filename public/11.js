(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[11],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Coupons.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Coupons.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! sweetalert2/dist/sweetalert2.js */ "./node_modules/sweetalert2/dist/sweetalert2.js");
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2/src/sweetalert2.scss */ "./node_modules/sweetalert2/src/sweetalert2.scss");
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      //   isLoggedIn : null,
      coupons: [],
      coupon: {
        id: '',
        coupon: '',
        valid_until: '',
        amount: '',
        percent: ''
      },
      edit: false
    };
  },
  mounted: function mounted() {//  this.isLoggedIn = localStorage.getItem('shop.mdi.in.rs.jwt') != null;
  },
  created: function created() {
    this.all();
  },
  methods: {
    all: function all() {
      var _this = this;

      var uri = '/api/coupons';
      this.axios.get(uri).then(function (response) {
        _this.coupons = response.data;
      });
    },
    deleteCoupon: function deleteCoupon(id) {
      var _this2 = this;

      var swalWithBootstrapButtons = sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      });
      swalWithBootstrapButtons.fire({
        title: 'Da li ste sigurni ?',
        text: "Želite da obrišete ovaj kupon!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Da, obriši ga!',
        cancelButtonText: 'Ne!',
        reverseButtons: true
      }).then(function (result) {
        if (result.value) {
          var uri = "/api/coupons/delete/".concat(id);

          _this2.axios["delete"](uri, {
            headers: {
              'Access-Control-Allow-Origin': '*'
            }
          }).then(function (response) {
            swalWithBootstrapButtons.fire('Obrisan!', 'Vaš kupon je obrisan.', 'success');

            _this2.clearForm();

            _this2.all();
          });
        } else if (
        /* Read more about handling dismissals below */
        result.dismiss === sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.DismissReason.cancel) {
          swalWithBootstrapButtons.fire('Otkazano', 'Vaš kupon je i dalje aktivan', 'info');
        }
      });
    },
    allCustomers: function allCustomers(id) {
      var _this3 = this;

      var uri = "/api/coupons/all/".concat(id);
      this.axios.get(uri).then(function (response) {
        _this3.clearForm();

        sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Kupon je dodeljen svim registrovanim kupcima!',
          showConfirmButton: false,
          timer: 1500
        });

        _this3.all();
      });
    },
    regularCustomers: function regularCustomers(id) {
      var _this4 = this;

      var uri = "/api/coupons/regular/".concat(id);
      this.axios.get(uri).then(function (response) {
        _this4.clearForm();

        sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Kupon je dodeljen redovnim kupcima!',
          showConfirmButton: false,
          timer: 1500
        });

        _this4.all();
      });
    },
    add: function add() {
      var _this5 = this;

      if (this.edit === false) {
        var uri = '/api/coupons/create';
        this.axios.post(uri, this.coupon).then(function (response) {
          _this5.clearForm();

          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Novi kupon je dodat!',
            showConfirmButton: false,
            timer: 1500
          });

          _this5.all();
        });
      } else {
        // update 
        var _uri = "/api/coupons/update/".concat(this.coupon.id);

        this.axios.post(_uri, this.coupon).then(function (response) {
          _this5.clearForm();

          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Podaci o kuponu su izmenjeni!',
            showConfirmButton: false,
            timer: 1500
          });

          _this5.all();
        });
      }
    },
    editCoupon: function editCoupon(coupon) {
      this.edit = true;
      this.coupon.id = coupon.id;
      this.coupon.coupon = coupon.coupon;
      this.coupon.valid_until = coupon.valid_until;
      this.coupon.amount = coupon.amount;
      this.coupon.percent = coupon.percent;
    },
    stopEdit: function stopEdit() {
      this.edit = false;
    },
    clearForm: function clearForm() {
      this.edit = false;
      this.coupon.id = null;
      this.coupon.coupon = null;
      this.coupon.valid_until = null;
      this.coupon.amount = null;
      this.coupon.percent = null;
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Coupons.vue?vue&type=template&id=4b0e4372&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Coupons.vue?vue&type=template&id=4b0e4372& ***!
  \****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container", attrs: { id: "rout" } }, [
    _c("br"),
    _c("br"),
    _c("br"),
    _vm._v(" "),
    _c("div", [
      _vm.edit
        ? _c("div", [_c("h1", [_vm._v("Update coupon:")])])
        : _c("div", [_c("h1", [_vm._v("Add Coupon:")])]),
      _vm._v(" "),
      _c(
        "form",
        {
          on: {
            submit: function($event) {
              $event.preventDefault()
              return _vm.add($event)
            }
          }
        },
        [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6" }, [
              _c("div", { staticClass: "form-group" }, [
                _vm._v("\n                            Name: "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.coupon.coupon,
                      expression: "coupon.coupon"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text" },
                  domProps: { value: _vm.coupon.coupon },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.coupon, "coupon", $event.target.value)
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _vm._v("\n                            Amount: "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.coupon.amount,
                      expression: "coupon.amount"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "number" },
                  domProps: { value: _vm.coupon.amount },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.coupon, "amount", $event.target.value)
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _vm._v("\n                            Percent: "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.coupon.percent,
                      expression: "coupon.percent"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "number" },
                  domProps: { value: _vm.coupon.percent },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.coupon, "percent", $event.target.value)
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c(
                "div",
                [
                  _c("label", { attrs: { for: "example-datepicker" } }, [
                    _vm._v("Valid Until:")
                  ]),
                  _vm._v(" "),
                  _c("b-form-datepicker", {
                    staticClass: "mb-2",
                    attrs: {
                      id: "example-datepicker",
                      placeholder: "Choose a date",
                      local: "en"
                    },
                    model: {
                      value: _vm.coupon.valid_until,
                      callback: function($$v) {
                        _vm.$set(_vm.coupon, "valid_until", $$v)
                      },
                      expression: "coupon.valid_until"
                    }
                  }),
                  _vm._v(" "),
                  _vm.edit
                    ? _c("p", { staticClass: "mb-1" }, [
                        _vm._v(
                          "Valid Until: '" +
                            _vm._s(_vm.coupon.valid_until) +
                            "'"
                        )
                      ])
                    : _vm._e()
                ],
                1
              )
            ])
          ]),
          _vm._v(" "),
          _c("br"),
          _vm._v(" "),
          _vm.edit
            ? _c("div", { staticClass: "form-group" }, [
                _c("button", { staticClass: "site-btn place-btn" }, [
                  _vm._v("Izmeni")
                ]),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass: "site-btn place-btn",
                    on: {
                      click: function($event) {
                        $event.preventDefault()
                        return _vm.clearForm()
                      }
                    }
                  },
                  [_vm._v("Povratak na prethodnu stranu")]
                )
              ])
            : _c("div", { staticClass: "form-group" }, [
                _c("button", { staticClass: "site-btn place-btn" }, [
                  _vm._v("Kreiraj")
                ])
              ])
        ]
      )
    ]),
    _vm._v(" "),
    _c("table", { staticClass: "table table-hover" }, [
      _vm._m(0),
      _vm._v(" "),
      _c(
        "tbody",
        _vm._l(_vm.coupons, function(coupon) {
          return _c("tr", { key: coupon.id }, [
            _c("td", [_vm._v(_vm._s(coupon.id))]),
            _vm._v(" "),
            _c("td", [_vm._v(_vm._s(coupon.coupon))]),
            _vm._v(" "),
            _c("td", [_vm._v(_vm._s(coupon.valid_until))]),
            _vm._v(" "),
            _c("td", [_vm._v(_vm._s(coupon.amount))]),
            _vm._v(" "),
            _c("td", [_vm._v(_vm._s(coupon.percent))]),
            _vm._v(" "),
            _c("td", [
              _c(
                "button",
                {
                  staticClass: "p-btn",
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.editCoupon(coupon)
                    }
                  }
                },
                [_vm._v("Edit")]
              ),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass: "p-btn",
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.deleteCoupon(coupon.id)
                    }
                  }
                },
                [_vm._v("Delete")]
              )
            ]),
            _vm._v(" "),
            _c("td", [
              _c(
                "button",
                {
                  staticClass: "p-btn",
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.allCustomers(coupon.id)
                    }
                  }
                },
                [_vm._v("Assign")]
              )
            ]),
            _vm._v(" "),
            _c("td", [
              _c(
                "button",
                {
                  staticClass: "p-btn",
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.regularCustomers(coupon.id)
                    }
                  }
                },
                [_vm._v("Assign")]
              )
            ])
          ])
        }),
        0
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("ID")]),
        _vm._v(" "),
        _c("th", [_vm._v("Coupon")]),
        _vm._v(" "),
        _c("th", [_vm._v("Valid Until")]),
        _vm._v(" "),
        _c("th", [_vm._v("Amount")]),
        _vm._v(" "),
        _c("th", [_vm._v("Percent")]),
        _vm._v(" "),
        _c("th", [_vm._v("Actions")]),
        _vm._v(" "),
        _c("th", [_vm._v("Assign to all customers")]),
        _vm._v(" "),
        _c("th", [_vm._v("Assign to regular customers")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/admin/Coupons.vue":
/*!***************************************************!*\
  !*** ./resources/js/components/admin/Coupons.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Coupons_vue_vue_type_template_id_4b0e4372___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Coupons.vue?vue&type=template&id=4b0e4372& */ "./resources/js/components/admin/Coupons.vue?vue&type=template&id=4b0e4372&");
/* harmony import */ var _Coupons_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Coupons.vue?vue&type=script&lang=js& */ "./resources/js/components/admin/Coupons.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Coupons_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Coupons_vue_vue_type_template_id_4b0e4372___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Coupons_vue_vue_type_template_id_4b0e4372___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/admin/Coupons.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/admin/Coupons.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/js/components/admin/Coupons.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Coupons_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Coupons.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Coupons.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Coupons_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/admin/Coupons.vue?vue&type=template&id=4b0e4372&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/admin/Coupons.vue?vue&type=template&id=4b0e4372& ***!
  \**********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Coupons_vue_vue_type_template_id_4b0e4372___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Coupons.vue?vue&type=template&id=4b0e4372& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Coupons.vue?vue&type=template&id=4b0e4372&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Coupons_vue_vue_type_template_id_4b0e4372___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Coupons_vue_vue_type_template_id_4b0e4372___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);