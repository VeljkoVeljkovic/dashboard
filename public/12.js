(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[12],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Tag.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Tag.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! sweetalert2/dist/sweetalert2.js */ "./node_modules/sweetalert2/dist/sweetalert2.js");
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2/src/sweetalert2.scss */ "./node_modules/sweetalert2/src/sweetalert2.scss");
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      //     isLoggedIn : null,
      tags: [],
      tag: {
        id: '',
        tag: ''
      },
      tag_id: '',
      edit: false
    };
  },
  mounted: function mounted() {//     this.isLoggedIn = localStorage.getItem('shop.in.rs.jwt') != null;
  },
  created: function created() {
    this.all();
  },
  methods: {
    stopEdit: function stopEdit() {
      this.edit = false;
    },
    all: function all() {
      var _this = this;

      var uri = '/api/tags';
      this.axios.get(uri).then(function (response) {
        _this.tags = response.data;
      });
    },
    deleteTag: function deleteTag(id) {
      var _this2 = this;

      var swalWithBootstrapButtons = sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      });
      swalWithBootstrapButtons.fire({
        title: 'Da li ste sigurni ?',
        text: "Želite da obrišete ovaj tag!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Da, obriši ga!',
        cancelButtonText: 'Ne!',
        reverseButtons: true
      }).then(function (result) {
        if (result.value) {
          var uri = "/api/tags/delete/".concat(id);

          _this2.axios["delete"](uri, {
            headers: {
              'Access-Control-Allow-Origin': '*'
            }
          }).then(function (response) {
            swalWithBootstrapButtons.fire('Obrisan!', 'Vaš tag je obrisan.', 'success');

            _this2.clearForm();

            _this2.all();
          });
        } else if (
        /* Read more about handling dismissals below */
        result.dismiss === sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.DismissReason.cancel) {
          swalWithBootstrapButtons.fire('Otkazano', 'Vaš tag je i dalje aktivan', 'info');
        }
      });
    },
    add: function add() {
      var _this3 = this;

      if (this.edit === false) {
        var uri = '/api/tags/create';
        this.axios.post(uri, this.tag).then(function (response) {
          _this3.clearForm();

          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Novi tag je dodat!',
            showConfirmButton: false,
            timer: 1500
          });

          _this3.all();
        });
      } else {
        // update 
        var _uri = "/api/tags/update/".concat(this.tag.id);

        this.axios.post(_uri, this.tag).then(function (response) {
          _this3.clearForm();

          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Podaci o tagu su izmenjeni!',
            showConfirmButton: false,
            timer: 1500
          });

          _this3.all();
        });
      }
    },
    editTag: function editTag(tag) {
      this.edit = true;
      this.tag.id = tag.id;
      this.tag.tag_id = tag.tag_id;
      this.tag.tag = tag.tag;
    },
    clearForm: function clearForm() {
      this.edit = false;
      this.tag.id = null;
      this.tag.tag_id = null;
      this.tag.tag = null;
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Tag.vue?vue&type=template&id=756e1402&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Tag.vue?vue&type=template&id=756e1402& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container", attrs: { id: "rout" } }, [
    _c("br"),
    _c("br"),
    _c("br"),
    _vm._v(" "),
    _c("div", [
      _vm.edit
        ? _c("div", [_c("h1", [_vm._v("Izmeni podatke o tagu:")])])
        : _c("div", [_c("h1", [_vm._v("Dodaj tag:")])]),
      _vm._v(" "),
      _c(
        "form",
        {
          on: {
            submit: function($event) {
              $event.preventDefault()
              return _vm.add($event)
            }
          }
        },
        [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6" }, [
              _c("div", { staticClass: "form-group" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.tag.tag,
                      expression: "tag.tag"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text" },
                  domProps: { value: _vm.tag.tag },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.tag, "tag", $event.target.value)
                    }
                  }
                })
              ])
            ])
          ]),
          _vm._v(" "),
          _c("br"),
          _vm._v(" "),
          _vm.edit
            ? _c("div", { staticClass: "form-group" }, [
                _c("button", { staticClass: "site-btn place-btn" }, [
                  _vm._v("Izmeni")
                ]),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass: "site-btn place-btn",
                    on: {
                      click: function($event) {
                        $event.preventDefault()
                        return _vm.clearForm()
                      }
                    }
                  },
                  [_vm._v("Povratak na prethodnu stranu")]
                )
              ])
            : _c("div", { staticClass: "form-group" }, [
                _c("button", { staticClass: "site-btn place-btn" }, [
                  _vm._v("Kreiraj")
                ])
              ])
        ]
      )
    ]),
    _vm._v(" "),
    _c("table", { staticClass: "table table-hover" }, [
      _vm._m(0),
      _vm._v(" "),
      _c(
        "tbody",
        _vm._l(_vm.tags, function(tag) {
          return _c("tr", { key: tag.id }, [
            _c("td", [_vm._v(_vm._s(tag.id))]),
            _vm._v(" "),
            _c("td", [_vm._v(_vm._s(tag.tag))]),
            _vm._v(" "),
            _c("td", [
              _c(
                "button",
                {
                  staticClass: "p-btn",
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.editTag(tag)
                    }
                  }
                },
                [_vm._v("Edit")]
              ),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass: "p-btn",
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.deleteTag(tag.id)
                    }
                  }
                },
                [_vm._v("Delete")]
              )
            ])
          ])
        }),
        0
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("ID")]),
        _vm._v(" "),
        _c("th", [_vm._v("Tag")]),
        _vm._v(" "),
        _c("th", [_vm._v("Actions")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/admin/Tag.vue":
/*!***********************************************!*\
  !*** ./resources/js/components/admin/Tag.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Tag_vue_vue_type_template_id_756e1402___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Tag.vue?vue&type=template&id=756e1402& */ "./resources/js/components/admin/Tag.vue?vue&type=template&id=756e1402&");
/* harmony import */ var _Tag_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Tag.vue?vue&type=script&lang=js& */ "./resources/js/components/admin/Tag.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Tag_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Tag_vue_vue_type_template_id_756e1402___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Tag_vue_vue_type_template_id_756e1402___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/admin/Tag.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/admin/Tag.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./resources/js/components/admin/Tag.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Tag_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Tag.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Tag.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Tag_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/admin/Tag.vue?vue&type=template&id=756e1402&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/admin/Tag.vue?vue&type=template&id=756e1402& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Tag_vue_vue_type_template_id_756e1402___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Tag.vue?vue&type=template&id=756e1402& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Tag.vue?vue&type=template&id=756e1402&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Tag_vue_vue_type_template_id_756e1402___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Tag_vue_vue_type_template_id_756e1402___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);