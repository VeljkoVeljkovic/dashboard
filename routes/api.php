<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

	Route::post('login', 'UserController@login');
	Route::post('register', 'UserController@register');
	Route::post('changepassword', 'UserController@changepassword');
	Route::post('users/rola','UserController@changerola');
	Route::get('/products', 'ProductController@index');
	Route::post('/upload-file', 'ProductController@uploadFile');
	Route::get('/products/{product}', 'ProductController@show');
	Route::delete('/products/delete/{id}', 'ProductController@delete');
	Route::post('/products/update/{id}', 'ProductController@update');
	Route::post('/products/parent/{id}', 'ProductController@categor');
	Route::post('/products/create', 'ProductController@store');

	Route::post('/category/create', 'CategoryController@store');
	Route::get('/categories','CategoryController@index');
	Route::post('/category/update/{id}', 'CategoryController@update');
	Route::delete('/category/delete/{id}', 'CategoryController@delete');

	

	 Route::post('/tags/create', 'TagController@store');
	Route::get('/tags','TagController@index');
	Route::post('/tags/update/{id}', 'TagController@update');
	Route::delete('/tags/delete/{id}', 'TagController@delete');
    
	 Route::post('/coupons/create', 'CouponController@store');
	Route::get('/coupons','CouponController@index');
	Route::post('/coupons/update/{id}', 'CouponController@update');
	Route::delete('/coupons/delete/{id}', 'CouponController@delete');
	Route::get('/coupons/all/{id}', 'CouponController@allcustomers');
	Route::get('/coupons/regular/{id}', 'CouponController@regularcustomers');
	
    Route::post('/brands/create', 'BrandController@store');
	     Route::get('/brands','BrandController@index');
         Route::post('/brands/update/{id}', 'BrandController@update');
	     Route::delete('/brands/delete/{id}', 'BrandController@delete');
		 Route::get('/users','UserController@index');
		Route::get('users/{user}','UserController@show');
	   
		Route::get('users/{user}/orders','UserController@showOrders');

		 Route::patch('orders/{order}/deliver','OrderController@deliverOrder');
		 Route::resource('/orders', 'OrderController');
		 
	Route::group(['middleware' => 'auth:api'], function(){
		
		 
		 
		 
         
		
		
		
	});

	Route::group(['middleware' => 'cors'], function() {
  


//Prodavnica
	Route::resource('/products', 'ProductController');
	Route::get('/', 'ProductFrontController@index');
	Route::get('/category/{id}', 'ProductFrontController@category');
	Route::get('/tag/{id}', 'ProductFrontController@tag'); 
	
	 /*Route::post('/filter', 'ProductFrontController@filter');
	Route::post('/check', 'CouponController@check');
	Route::post('/order', 'OrderController@order');
	Route::post('/contact', 'CommentController@store');  */
	
	 //  Dashboard

     Route::get('/sales', 'ChartController@sales');
	 Route::get('/usery', 'ChartController@usery');
	 Route::get('/pie', 'ChartController@pie');
	 Route::get('/geo', 'ChartController@geo');
	 Route::get('/usersall', 'ChartController@index');
	 Route::get('/low-stock', 'ProductFrontController@lowstock');
	});
 
