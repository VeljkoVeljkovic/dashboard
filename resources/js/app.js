import Vue from 'vue'
 import 'es6-promise/auto';

import AOS from 'aos';
import 'aos/dist/aos.css'; 
import VueRouter from 'vue-router'
Vue.use(VueRouter)
import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios); 

import VueSweetalert2 from 'vue-sweetalert2';

Vue.use(VueSweetalert2);  

import { store } from './store/index'; 

import JwPagination from 'jw-vue-pagination';
Vue.component('jw-pagination', JwPagination);

//import Vuelidate from 'vuelidate'
//Vue.use(Vuelidate)


import Veevalidate from 'vee-validate';
Vue.use(Veevalidate, { fieldsBagName: 'veeFields' });



//Bootrsap vue
import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue) 
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(VueLazyload)
import App from './App.vue';
Vue.component('v-select', VueSelect.VueSelect);
 
// import VueGoogleCharts from 'vue-google-charts'
// Vue.use(VueGoogleCharts)
 
// import VueCharts from 'vue-chartjs';


 // Vue.use(VueCharts);
  import Chart from 'chart.js'
 import Chartkick from 'vue-chartkick'
 
 Vue.use(Chartkick)
 
 
const router = new VueRouter({
	mode: 'history',
	routes: [
 
		{
			path: '/login',
			name: 'login',
			// component: HomeComponent
			component: () => import('./components/Login')
		},
		{
		name: 'changepassword',
		path: '/changepassword',
		component: () => import('./components/ChangePassword.vue')
	},
		{
			path: '/register',
			name: 'register',
			// component: HomeComponent
			component: () => import('./components/Register')
		},
		
		{
			path: '/charts',
			name: 'charts',
			// component: HomeComponent
			component: () => import('./components/front/charts')
		},
		{
			path: '/tables',
			name: 'tables',
			// component: HomeComponent
			component: () => import('./components/front/tables')
		},
	 
		  {
			name: 'home',
			path: '/',	
			component: () => import('./components/front/home'),		
		},
		  {
			name: 'sales',
			path: '/sales',	
			component: () => import('./components/front/sales'),		
		},
		
		 {
			name: 'low-stock',
			path: '/low-stock',	
			component: () => import('./components/front/low-stock'),		
		},
	  
        {   
  		  name: 'brands',
          path: '/brands',
          component: () => import('./components/admin/Brand.vue'),   
		  meta: {
			    requiresAuth: true,
				is_admin: true
			}		
        },
		{  
		  name: 'categories',
          path: '/categories',
          component: () => import('./components/admin/Category.vue'),
          meta: {
			    requiresAuth: true,
				is_admin: true
			}				  
        },
		{ 
		  name:  'coupons',     
          path: '/coupons',
          component: () => import('./components/admin/Coupons.vue'),
          meta: {
			    requiresAuth: true,
				is_admin: true
			}				  
        },
		{  
          name: 'orders',		
          path: '/orders',
          component: () => import('./components/admin/Orders.vue'), 
          meta: {
			    requiresAuth: true,
				is_admin: true
			}		  
        },
		{ 
		  name: 'products',     
          path: '/products',
          component: () => import('./components/admin/Products.vue'),  
          meta: {
			    requiresAuth: true,
				is_admin: true
			}		  
        },
		{ 
          name:	'tags',	
          path: '/tags',
          component: () => import('./components/admin/Tag.vue'),  
		  meta: {
			    requiresAuth: true,
				is_admin: true
			}
        },
		{
		  name:	'users',	
          path: '/users',
          component: () => import('./components/admin/Users.vue'),   
		  meta: {
			    requiresAuth: true,
				is_admin: true
			}
        },

	
	],
})

router.beforeEach((to, from, next) => {
	window.previousUrl = from.path
	if (to.matched.some(record => record.meta.requiresAuth)) {
		if (localStorage.getItem('dashboard.mdi.in.rs.jwt') == null) {
			next({
				path: '/login',
				params: { nextUrl: to.fullPath }
			})
		} else {
			let user = JSON.parse(localStorage.getItem('dashboard.mdi.in.rs.user'))
			if (to.matched.some(record => record.meta.is_admin)) {
				if (user.is_admin == 1) {
					next()
				}
				else {

					next({ name: 'home' })
				}
			}
			else if (to.matched.some(record => record.meta.is_user)) {
				if (user.is_admin == 0) {
					next()
				}
				else {
					next({ name: 'home' })
				}
			}
			next()
		}
	} else {
		next()
	}
})

/* const app = new Vue({
	el: '#app',
	components: { App },
	router,
}); */ 
new Vue({
	el: "#app",
	created() {
		AOS.init()
	}, 
	store,
	router: router,
	render: h => h(App)
})
