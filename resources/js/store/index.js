import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from "vuex-persistedstate";
import getters from './getters';
import actions from './actions';
import mutations from './mutations';


Vue.use(Vuex);

export const store = new Vuex.Store({
	plugins: [createPersistedState()],
	state: {
	message: '',
	alreadyInCart: false,
	cart: [],
	likes: [],
	categories:[],
	tags:[],
	brands:[],
	products:[],
	notfilteredproducts:[],
	user:[],
	coupons:[]
    },
	
	actions,
	mutations,
	getters
});