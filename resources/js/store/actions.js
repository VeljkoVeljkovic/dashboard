export default {
	addLike({ commit }, payload){
		setTimeout(()=> {
			commit('addLike', payload);
        },1000);
		
	},
	
	addCart({ commit }, payload) {
		setTimeout(()=> {
			commit('addCart', payload);
			
        },1000);
		
	},
	Cart({ commit }, payload) {
		setTimeout(()=> {
			commit('Cart', payload);
			
        },1000);
		
	},
	alreadyInCart({ commit }, payload) {
		
			commit('alreadyInCart', payload);			     		
	},
	deleteCart({ commit }) {
		setTimeout(()=> {
			commit('deleteCart');
        },1000);
		
	},
	notfilteredproducts({ commit }) {
		commit('notfilteredproducts')
	},
	changeQuantity({ commit }, payload) {
		setTimeout(()=> {
			commit('changeQuantity', payload);
        },1000);
		
	},
	couponData({ commit }, payload) {
		setTimeout(()=> {
			commit('couponData', payload);
        },1000);
		
	},
	discountCart({ commit }, payload) {
		setTimeout(()=> {
			commit('discountCart', payload);
        },1000);
		
	},
	User({ commit }, payload) {
		setTimeout(()=> {
			commit('UserData', payload);
        },1000);
		
	},
	discountCoupon({ commit }) {
		setTimeout(()=> {
			commit('discountCoupon');
        },1000);
		
	},
	remove({ commit }, payload) {
		setTimeout(()=> {
			commit('removeProduct', payload);
        },1000);
		
	},
	
    async start({ commit }) {
	let uri = '/api';
    const response = await axios.get(
     uri,  {
						headers: {
							'Access-Control-Allow-Origin': '*',
						}
					});
    commit('allData', response.data);
  },
  
   async tagovi({ commit }, id) {
	let uri = `/api/tag/${id}`;
    const response = await axios.get(
     uri,  {
						headers: {
							'Access-Control-Allow-Origin': '*',
						}
					});
    commit('allTag', response.data);
  },
   async category({ commit }, id) {
	let uri = `/api/category/${id}`;
    const response = await axios.get(
     uri,  {
						headers: {
							'Access-Control-Allow-Origin': '*',
						}
					});
    commit('category', response.data);
  },
	filter({ commit }, payload) {
		setTimeout(()=> {
			commit('filter', payload);
        },1000);
		
	},
}