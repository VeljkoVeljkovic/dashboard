//../src/Chart.js
    import {
		Pie,
        mixins
    } from 'vue-chartjs'

    export default {
        extends: Pie,
        mixins: [mixins.reactiveProp],
        props: ['chartData'],
        data() {
            return {
                options: {

                   legend: {
                        display: true,
						position: 'bottom'
                    },
                    responsive: true,
                    maintainAspectRatio: false					
				   /*  title:{
					  display:true,
					  text:'Largest Cities In Massachusetts',
					  fontSize:25
					},
					legend:{
					  display:true,
					  position:'right',
					  labels:{
						fontColor:'#000'
					  }
					},
					layout:{
					  padding:{
						left:50,
						right:0,
						bottom:0,
						top:0
					  }
					},
					tooltips:{
					  enabled:true
					} */
                    }
                    
                }
            },
       
        mounted() {
            this.renderChart(this.chartdata, this.options)
        }

    }